import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

//import 'package:shimmer/shimmer.dart';
import 'package:shimmer_example/core/widgets/shimmer.dart';

class ShimmerNetworkImage extends StatefulWidget {
  final String imageUrl;

  const ShimmerNetworkImage({
    super.key,
    required this.imageUrl,
  });

  @override
  State<ShimmerNetworkImage> createState() => ShimmerNetworkImageState();
}

class ShimmerNetworkImageState extends State<ShimmerNetworkImage> {
  bool isLoading = true;

  Widget buildWrapped(BuildContext context) {
    return ShimmerLoading(
      isLoading: isLoading,
      child: CachedNetworkImage(
        imageUrl: widget.imageUrl,
        progressIndicatorBuilder: (context, url, progress) {
          if (progress.progress == 1) {

          }
          return Container();
        },
      ),
    );
  }

  Widget buildUnwrapped(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: widget.imageUrl,
      placeholder: (context, url) {
        return ShimmerLoading(
            isLoading: true,
            child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
              ),
            ));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildUnwrapped(context);
    // return ShimmerLoading(
    //         isLoading: true,
    //         child: Container(decoration: const BoxDecoration(
    //           color: Colors.black,
    //         ),),
    //       );
    // return CachedNetworkImage(
    //   imageUrl: widget.imageUrl,
    // progressIndicatorBuilder: (context, url, progress) {
    //   if (context.mounted) {
    //     return ShimmerLoading(
    //         isLoading: true,
    //         child: Container(
    //           decoration: const BoxDecoration(
    //             color: Colors.black,
    //           ),
    //         ));
    //   } else {
    //     return Container();
    //   }
    // },
    //fadeInDuration : const Duration(milliseconds: 400),
    //placeholderFadeInDuration: const Duration(milliseconds: 0),
    /*placeholder: (context, url) => Shimmer.fromColors(
        baseColor: Color(0xFFEBEBF4),
        highlightColor: Color(0xFFF4F4F4),
        child: Container(
          color: Color(0xFFEBEBF4),
        ),
      ),*/
    /*imageBuilder: (context, imageProvider) {
        return Container(
          decoration: BoxDecoration(
            image: DecorationImage(image: imageProvider),
          ),
        );
        //return DecorationImage(image: imageProvider);
      },*/

    //
    //   fadeInDuration: Duration(seconds: 0),
    // );
  }
}
