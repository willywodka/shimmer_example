import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:shimmer_example/core/widgets/network_image.dart';
import 'package:shimmer_example/features/main_page/bloc/main_bloc.dart';
import 'package:shimmer_example/repository/dog_image_repository.dart';

class MainPage extends StatefulWidget {
  const MainPage({
    super.key,
  });

  @override
  State<MainPage> createState() => MainPageState();
}

class MainPageState extends State<MainPage> {

  bool _isLoading = true;

  void _toggleLoading() {
    setState(() {
      _isLoading = !_isLoading;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MainBloc(
          pageState: PageState(),
          dogImageRepository: context.read<GetIt>().get<DogImageRepository>()),
      child: BlocConsumer<MainBloc, MainState>(
        listener: (context, state) {
          // TODO: implement listener
        },
        builder: (context, state) {
          return Scaffold(
              /*Shimmer(
        linearGradient: _shimmerGradient,
        child: ListView(
          physics: _isLoading ? const NeverScrollableScrollPhysics() : null,
          children: [
            const SizedBox(height: 16),
            //_buildTopRowList(),
            const SizedBox(height: 16),
            _buildListItem(),
            //_buildListItem(),
            //_buildListItem(),
          ],
        ),*/
              body: state.pageState.dogImage.isNotEmpty
                  ? /*Shimmer(
                      linearGradient: _shimmerGradient,
                      child:*/ GridView.count(
                        crossAxisCount: 3,
                        children: state.pageState.dogImage
                            .map(
                              (e) => ShimmerNetworkImage(
                                imageUrl: e.message,
                                // placeholder: (context, url) => ShimmerLoading(
                                //   isLoading: true,
                                //   child: Container(
                                //     decoration: const BoxDecoration(color: Colors.black),
                                //     height: 50,
                                //     width: 50,
                                //   ),
                                // ),
                              ),
                            )
                            .toList() /*[
                    CachedNetworkImage(
                      imageUrl: state.pageState.dogImage.first.message,
                      placeholder: (context, url) {
                        return const CircularProgressIndicator();
                      },
                    )
                    //_buildListItem(),
                    //_buildListItem(),
                    //_buildListItem(),
                    //_buildListItem(),
                  ]*/
                        ,
                      )
                   // )
                  : Container(),
              /*
              state.pageState.dogImage.isNotEmpty
                  ? CachedNetworkImage(imageUrl: state.pageState.dogImage.first.message)
                  : const SizedBox(),
                  */
              floatingActionButton: FloatingActionButton(
                onPressed: _toggleLoading,
                child: Icon(
                  _isLoading ? Icons.hourglass_full : Icons.hourglass_bottom,
                ),
              ));
        },
      ),
    );
  }
}
