
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:shimmer_example/model/dog_image.dart';
import 'package:shimmer_example/repository/dog_image_repository.dart';

part 'main_event.dart';

part 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  final DogImageRepository dogImageRepository;

  MainBloc({
    required PageState pageState,
    required this.dogImageRepository,
  }) : super(MainInitial(pageState)) {
    on<MainEvent>((event, emit) {});
    on<MainInitEvent>(init);
    add(MainInitEvent());
  }

  init(MainInitEvent event, emit) async {
    final List<DogImage> list = await dogImageRepository.fetchMultipleImages(9);
    emit(MainUp(state.pageState.copyWith(dogImage: list)));
  }


}
